local move = require("move")
local gameConfig = require("gameConfig")
local Sublevel = require("Sublevel")
local Rock = require("asteroids.BrownBig1")
local GravityField = require("GravityField")
local EnemyPlane1 = require("levels.0324087.EnemyPlane1")
local EnemyPlane2 = require("levels.0324087.EnemyPlane2")
local EnemyPlane3 = require("levels.0324087.EnemyPlane3")
local EnemyPlane4 = require("levels.0324087.EnemyPlane4")
local EnemyPlane5 = require("levels.0324087.EnemyPlane5")
local MyEnemy = require("levels.0324087.Boss")
local HpBar = require("ui.HPBar")
local util = require("util")
local sublevel = Sublevel.new("9999999-087", "凃翰穎", "MyLevel")


function sublevel:prepare()
    self.bossInited = false
    self._myFinishFlag = false
end

function sublevel:show(options)
    local EnemyPlane = {EnemyPlane1,EnemyPlane2,EnemyPlane3,EnemyPlane4,EnemyPlane5}
    local upitem = {"levels.0324087.Item1","levels.0324087.Item2","levels.0324087.Item3","levels.0324087.Item4","levels.0324087.Item5"}
    local gear = {"levels.0324087.MyGearItem1","levels.0324087.MyGearItem2"}
    local field = GravityField.new({radius = 160 *
    gameConfig.scaleFactor})
    field.x = gameConfig.contentCenterX
    field.y = gameConfig.contentCenterY
    self:insert(field)
    if self.gameMode == gameConfig.MODE_INFINITE_LEVEL  then
        self:addTimer(20000, function()
            self._myFinishFlag = true
        end)
    elseif self.gameMode == gameConfig.MODE_SINGLE_LEVEL then
        self:addTimer(20000, function()
          self:showWarning({
            bg = "bg2",
            onComplete = function()
              self:initBoss()
            end
          })
        end)
    end
    self:addTimer(0, function()
        local enemy = EnemyPlane[1].new()
        enemy.x = 0
        enemy.y = gameConfig.contentHeight
        transition.to(enemy, {x = gameConfig.contentWidth/2, y = 0, time = 1500 ,onComplete =
            function()
                transition.to(enemy, {x = gameConfig.contentWidth-enemy.width, y = gameConfig.contentHeight/2, time = 1000,onComplete =
                    function()
                      enemy:setLinearVelocity( -enemy.width, gameConfig.contentHeight )
                    end})
            end
        })

        enemy:addItem(gear[1])
        enemy.immuneGravityField = false
        enemy:autoDestroyWhenInTheScreen()
        self.view:insert(enemy)

    end)
    self:addTimer(0, function()
        local enemy = EnemyPlane[1].new()
        enemy.x = gameConfig.contentWidth
        enemy.y = gameConfig.contentHeight
        transition.to(enemy, {x = gameConfig.contentWidth/2, y = 0, time = 1500 ,onComplete =
            function()
                transition.to(enemy, {x = enemy.width, y = gameConfig.contentHeight/2, time = 1000,onComplete =
                    function()
                      enemy:setLinearVelocity( gameConfig.contentWidth+enemy.width, gameConfig.contentHeight )
                    end})
            end
        })

        enemy:addItem(gear[2])
        enemy.immuneGravityField = false
        enemy:autoDestroyWhenInTheScreen()
        self.view:insert(enemy)

    end)
    for j = 2, 10 do
      self:addTimer(1500 * j, function()
          local rock = Rock.new()
          enemy = EnemyPlane[2].new()
          math.randomseed(j)
          local pathPoints = move.getCurve(
          {
              {x = gameConfig.contentWidth*math.random(3,8)/10, y = 60}, --P1
              {x = gameConfig.contentWidth*math.random(1,9)/10, y = gameConfig.contentWidth*math.random(5,10)/10}, --P2
              {x = gameConfig.contentWidth*math.random(1,9)/10, y = gameConfig.contentHeight*math.random(1,9)/10}, --P3
              {x = -gameConfig.contentWidth*math.random(-9,9)/10, y = gameConfig.contentHeight+enemy.height} --P4
          }, 100)
          rock.hp = 21
          enemy.x = pathPoints[1].x
          enemy.y = pathPoints[1].y
          move.followN(enemy, pathPoints, {
              showPoints = false,
              speed = 250 * gameConfig.scaleFactor,
              autoRotation = true,
              idep = false,
              onComplete = function()
                  enemy:clear()
              end
          })
          move.rotateAround(rock, {
              target = enemy,
              speed = 5,
              distance = 75 * gameConfig.scaleFactor,
              startDegree = 30,
              onMissTarget = function(angle)
                move.seek(rock, self.player, {
                    maxForce = 10
                })
              end
          })
          if i == num then
              enemy:addItem("items.PowerUp", {level = 1})
          end
          self.view:insert(rock)
          self.view:insert(enemy)
          enemy:addItem(upitem[math.random(5,5)], {level = 1})
          enemy:autoDestroyWhenInTheScreen()
      end)
  end
end

function sublevel:initBoss()
    local enemy = MyEnemy.new({players = self.players})
    --Set the boss to be invisible at the beginning
    enemy.invincible = true
    self.bossInited = true
    --set up hp bar
    local hpBar = HpBar.new({
        w = gameConfig.contentWidth*0.88,
        h = gameConfig.contentWidth*0.1,
        numOfLifes = 10,
        title = "Boss2"
    })
    hpBar.x = gameConfig.contentWidth / 2
    hpBar.y = hpBar.height * 0.6
    hpBar:update(enemy.hp , enemy.maxHp)
    --end of setting up hp bar
    --place the enemy out of the screen
    enemy.x = gameConfig.contentWidth/2
    enemy.y = -enemy.height/2
    --add Item to the enemy
    enemy:addItem("items.PowerUp", {level = 1})
    --destroy the enemy at the right time
    enemy:autoDestroyWhenInTheScreen()

    local function checkHPBar(event)
        if util.isExists(hpBar) then
            hpBar:update(enemy.hp, enemy.maxHp)
        end
    end
    --update hp bar when enemy is hurt
    enemy:addEventListener("health", checkHPBar)
    --hide the score via the game controller
    self.game:showScore(false)

    enemy:setScaleLinearVelocity(0, 100)
    enemy:addTimer(2000, function()
        enemy:setScaleLinearVelocity(0, 0)
        --When the enemy is ready, the player can hurt it
        enemy.invincible = false
        enemy:startAction()
    end)
    self.hpBar = hpBar
    self.enemy = enemy
    --insert to the scene
    self:insert(hpBar)
    self:insert(enemy)
end

function sublevel:finish()
    self.game:showScore(true)
    if self.hpBar then
        self.hpBar:clear()
    end
    self.bossInited = false
end

function sublevel:isFinish()
    --print("isFinish!??")
    if self.gameMode == gameConfig.MODE_INFINITE_LEVEL then
        return self._myFinishFlag
    elseif self.gameMode == gameConfig.MODE_SINGLE_LEVEL  then
        if not self.bossInited or util.isExists(self.enemy) then
            return false
        else
            return true
        end
    end
end

return sublevel
