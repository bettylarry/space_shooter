local Bullet = require("Bullet")
local Sprite = require("Sprite")
local Laser = {}

Laser.new = function(options)
    local laser = Bullet.new(options)
    local sprite = Sprite.new("Missiles/4")
    laser.name = "missile"
    laser:insert(sprite)
    laser:enablePhysics()
    return laser
end

return Laser
