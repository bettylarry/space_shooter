local Gear = require("Gear")
local Sprite = require("Sprite")
local gameConfig = require("gameConfig")

local MyGear = {}

MyGear.new = function(options)
    local myGear = Gear.new(options)
    myGear.dir = 90
    myGear.gearId = "0324087-1"

    local gun1 = Sprite["expansion-3"].new("Ships/28")
    local gun2 = Sprite["expansion-3"].new("Ships/28")

    local receiver = options.receiver

    myGear:insert(gun1)
    myGear:insert(gun2)
    gun1.x = -receiver.width
    gun2.x = receiver.width

    myGear:setDefaultBullet("levels.0324087.Bullet1")

    myGear:addTimer(600, function()
        myGear:shoot({
            x = myGear.x + receiver.width,
            degree = 90,
            speed = 500 * gameConfig.scaleFactor

        })
        myGear:shoot({
            x = myGear.x - receiver.width,
            degree = 90,
            speed = 500 * gameConfig.scaleFactor
        })
    end, -1)

    return myGear
end
return MyGear
